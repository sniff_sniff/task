# TASK 
1) PCA from Scratch using Numpy and Pandas (reduce to 2 Dimensions)
2) Bowling Pins from hackerrank

## PCA from scratch

### Prerequisites

1) python3 : sudo apt-get install python3.6 

2) Numpy : pip install numpy

3) pandas: pip install pandas

4) matplotlib : pip install matplotlib


### Data Visualization

784 Dimensions reduced to 2 Dimension (9 Labels)

<p align="center"> <img src="https://gitlab.com/sniff_sniff/task/-/blob/master/Machine_Learning/output/PCA.png" alt="MNIST Dimensionality reduction"/> </p>



## Bowling Pins

Passed all Test casses 

