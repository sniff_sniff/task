#!/bin/python3

import os
import sys
from functools import wraps

def reduce_time_complexity(function):
    memo = {}

    @wraps(function)
    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv
    return wrapper

"""With Minimum Excluded Ordinal we find the 
   smallest value from the set which is not part of the subset""" 
def mex(p):
    minimum_value = 0
    while minimum_value in p: 
        minimum_value += 1
    return minimum_value

@reduce_time_complexity
def positions(move):
    possible_position = set()
    # predicting the positions after a Move 
    # One Pin Hit
    for i in range(move):
        possible_position.add(positions(i) ^ positions(move - 1 - i))
    # Two Pin Hit    
    for i in range(move - 1):
        possible_position.add(positions(i) ^ positions(move - 2 - i))
    # Calculating minimum excluded ordinal to calculate Nim 
    return mex(possible_position)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    for testcases in range(int(input())):# Iterating with Number of Games 
        # Length of Pins
        input()
        x = 0
        
        # PINS 
        pins = input()
        
        # Split the string with "X"
        pins_splitted = pins.split('X')
        
        # Empty space Removal
        sizes_of_pin =  [x for x in pins_splitted if x]
        
        # Iterating through size of pins and finding the length of adjacent pins
        for pin in sizes_of_pin:
            # Iterating through available pins and finding the Nim Pile(XOR)
            x ^= positions(len(pin)) 
         # If Nimpile != 0 , Its a Winning position
        if x != 0:
            result = "WIN"
        else:
            result = "LOSE" 

        fptr.write(result + '\n')

    fptr.close()

